const burger = document.getElementsByClassName('fork-burger')[0];
const dropdown = document.getElementsByClassName("fork-burger-dropdown")[0];

burger.onclick = () =>{
    burger.classList.toggle('burger-toggle');
    dropdown.classList.toggle("dropdown-active");
};
